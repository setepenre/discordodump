# discordodump

dumps discord server messages to stdout in dht json format

## setup

uses python >= 3.8.5

```bash
    $ git clone git@framagit.org:setepenre/discordodump.git
    $ cd discordodump
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt
```

## usage

```bash
    $ ./discordodump.py -h
    $ ./discordodump.py "token" "server name" > dump.json
    $ ./discordodump.py "token" "server name" --before "2020-04-20 13:12:00" > dump.json
    $ ./discordodump.py "token" "server name" --after "2020-04-20 13:12:00" > dump.json
    $ ./discordodump.py "token" "server name" --after 1936-01-01 --before "2020-04-20 13:12:00" > dump.json
```

regular usage should display INFO logs, list the channels it's dumping then exit

resulting dumps can be merged using `tools/mergedumps`:
```bash
    $ ./tools/mergedumps.py dump_0.json dump_1.json ... > merge.json
```

## development

- uses black for styling
- flake8 conf comes with .flake8 file

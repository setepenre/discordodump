#!/usr/bin/env python3

import argparse
import datetime
from functools import reduce
import json
import logging
from time import mktime
from typing import Any, List

import discord


logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(
    description="dump message from server [server_name] using [token]"
)
parser.add_argument("token", type=str, help="discord user / bot token")
parser.add_argument("server_name", type=str, help="server name")
parser.add_argument(
    "--before",
    type=datetime.datetime.fromisoformat,
    help="select messages before date in ISO format (YYYY-MM-DD HH:MM:SS)",
)
parser.add_argument(
    "--after",
    type=datetime.datetime.fromisoformat,
    help="select messages after date in ISO format (YYYY-MM-DD HH:MM:SS)",
)
args = parser.parse_args()

client = discord.Client()


def message(m: discord.Message) -> List[Any]:
    return [m.author.name, m.clean_content, str(m.created_at), str(m.edited_at)]


@client.event
async def on_ready():
    logging.info("logged in as {0}".format(client.user))
    if not (
        server := discord.utils.find(
            lambda g: g.name == args.server_name, client.guilds
        )
    ):
        return

    channels = list(
        filter(
            lambda c: c.permissions_for(server.me).read_message_history
            and type(c) == discord.TextChannel,
            server.channels,
        )
    )

    all_messages = {}
    for channel in channels:
        logging.info(
            "retrieving messages for {0} - {1}".format(channel.guild, channel.name)
        )
        messages = (
            await channel.history(limit=None, before=args.before, after=args.after)
            .filter(lambda m: m.type == discord.MessageType.default)
            .map(
                lambda m: {
                    f"{m.id}": {
                        "author": m.author,
                        "t": mktime(m.created_at.timetuple()) * 1000,
                        "m": m.clean_content,
                        "a": [
                            {"url": embed.url, "type": embed.type}
                            for embed in m.embeds
                            if embed.url != discord.Embed.Empty
                        ],
                    }
                }
            )
            .flatten()
        )
        all_messages[f"{channel.id}"] = reduce(lambda a, b: {**a, **b}, messages, {})

    authors = set(
        [
            d["author"]
            for _, messages in all_messages.items()
            for _, d in messages.items()
        ]
    )
    users = {f"{author.id}": {"name": author.name} for author in authors}
    userindex = list(users.keys())

    dht = {
        "meta": {
            "users": users,
            "userindex": userindex,
            "servers": [{"name": server.name, "type": "SERVER"}],
            "channels": {f"{c.id}": {"server": 0, "name": c.name} for c in channels},
        },
        "data": {
            channel_id: {
                message_id: {
                    **{"u": userindex.index(f"{message['author'].id}")},
                    **{k: v for k, v in message.items() if k in "tma" and v},
                }
                for message_id, message in messages.items()
            }
            for channel_id, messages in all_messages.items()
        },
    }

    print(json.dumps(dht))
    await client.close()


client.run(args.token, bot=False)

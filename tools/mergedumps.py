#!/usr/bin/env python3

import argparse
from functools import reduce
import json
from typing import Any, List


parser = argparse.ArgumentParser(description="merge discordodump files")
parser.add_argument("files", type=str, nargs="+", help="files to merge")


def naive_list_join(left: List[Any], right: List[Any]) -> List[Any]:
    return left + [_ for _ in right if _ not in left]


def get_value_by_key_with_default(key: Any, default: Any, d: dict) -> Any:
    return d[k] if k in d else default


def join(left: dict, right: dict) -> dict:
    return {
        "meta": {
            "users": (users := {**left["meta"]["users"], **right["meta"]["users"]}),
            "userindex": list(users.keys()),
            "servers": naive_list_join(
                left["meta"]["servers"], right["meta"]["servers"]
            ),
            "channels": {**left["meta"]["channels"], **right["meta"]["channels"]},
        },
        "data": {
            k: {
                **get_value_by_key_with_default(k, {}, left["data"]),
                **get_value_by_key_with_default(k, {}, right["data"]),
            }
            for k in set(list(left["data"].keys()) + list(right["data"].keys()))
        },
    }


if __name__ == "__main__":
    args = parser.parse_args()

    print(
        json.dumps(reduce(join, map(lambda f: json.loads(open(f).read()), args.files)))
    )
